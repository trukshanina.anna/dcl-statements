/* 1.Create a new user with the username "rentaluser" and the password "rentalpassword". 
 Give the user the ability to connect to the database but no other permissions. */
 
 create user rentaluser with password 'rentalpassword';
 grant connect on database dvdrental to rentaluser;
 
/* 2. Grant "rentaluser" SELECT permission for the "customer" table. 
Сheck to make sure this permission works correctly—write a SQL query to select all customers.*/

  grant select on table customer to rentaluser;

  set role rentaluser;
  select * from customer;
  reset role;
	
/* 3. Create a new user group called "rental" and add "rentaluser" to the group */

  create group rental;
  grant rental to rentaluser;

/* 4. Grant the "rental" group INSERT and UPDATE permissions for the "rental" table.
Insert a new row and update one existing row in the "rental" table under that role. */ 

 grant insert, select, update on table rental to rental;

 set role rentaluser;
 show role;
 
 insert into rental (rental_date, inventory_id, customer_id, return_date, staff_id, last_update)
 values (CURRENT_DATE , 111, 222, CURRENT_DATE , 3, NOW());
 
 update rental set return_date = '2024-02-04' where rental_id = 1;
 
 reset role;
 
/* 5. Revoke the "rental" group's INSERT permission for the "rental" table. 
Try to insert new rows into the "rental" table make sure this action is denied.*/

  revoke insert on table rental from rental;

  set role rentaluser;
  show role;
  
  insert into rental(rental_date,inventory_id , customer_id, return_date,staff_id ,last_update)
  values(CURRENT_DATE , 333, 444, CURRENT_DATE, 3, NOW());
  
  reset role;
  
/* 6. Create a personalized role for any customer already existing in the dvd_rental database.
The name of the role name must be client_{first_name}_{last_name} (omit curly brackets). 
The customer's payment and rental history must not be empty. 
Configure that role so that the customer can only access their own data in the 
"rental" and "payment" tables. Write a query to make sure this user sees only their own data. */

create role client_judy_gray login password 'password_for_judy_gray';

  create policy select_own_data_policy1 ON rental
  for select to client_judy_gray
  using (customer_id = (select customer_id from customer where first_name = 'JUDY' and last_name = 'GRAY'));
  
  create policy  select_own_data_policy2 ON payment
  for select to client_judy_gray
  using (customer_id = (select customer_id from customer where first_name = 'JUDY' and last_name = 'GRAY'));
  
  create policy  select_own_data_policy3 ON customer
  for select to client_judy_gray
  using (false);
  
grant select on table rental, payment, customer to client_judy_gray;
 
set role client_judy_gray;

select * from rental;
select * from payment;
  
reset role 
